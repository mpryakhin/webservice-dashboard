# Webservice Dashboard

## Test server

Run `docker-compose -f docker-compose.test.yaml up` for a test server.

Navigate to `http://app.localhost`

## Development server

Run `docker-compose run app npm install` to install project dependencies.

Run `docker-compose up` for a dev server.

The application will automatically reload if you change any of the source files

## Build

Run `docker build -t $IMAGE:$TAG .` to build the project. 

The build artifacts will be stored in the `/app` image directory
