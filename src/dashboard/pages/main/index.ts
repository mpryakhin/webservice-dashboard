import { Component } from '@angular/core';

@Component({
    selector: 'main-page',
    templateUrl: 'template.html',
})
export class MainPage {
    public name: string = 'World';
}
