import { Error404Page } from './error-404';
import { MainPage } from './main';

export {
    Error404Page,
    MainPage,
}
