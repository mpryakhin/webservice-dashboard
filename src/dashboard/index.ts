import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationComponent } from './components';
import { Error404Page, MainPage } from './pages';

const routes: Routes = [
    { path: '', pathMatch: 'full', component: MainPage },
    { path: '**', component: Error404Page },
];

@NgModule({
    bootstrap: [
        ApplicationComponent,
    ],
    declarations: [
        ApplicationComponent,
        Error404Page,
        MainPage,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [],
})
export class DashboardModule {
}
