import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DashboardModule } from './dashboard';

platformBrowserDynamic()
    .bootstrapModule(DashboardModule)
    .catch(err => console.error(err));
