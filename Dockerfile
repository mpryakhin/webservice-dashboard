########################################
# Node.js stage
########################################
FROM node:18-alpine AS app
WORKDIR /app

# Install angular cli
RUN npm install -g @angular/cli@17.3.6

# Install project dependencies
COPY angular.json package.json package-lock.json ./
RUN npm install

# Copy project files
COPY src src/
COPY tsconfig.json ./

# Compile project
RUN ng build

########################################
# NGINX stage
########################################
FROM nginx:1.26
WORKDIR /app

# Copy project files
COPY .docker/nginx.conf /etc/nginx/templates/default.conf.template
COPY --from=app /app/dist/dashboard ./
